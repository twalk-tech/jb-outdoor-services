/* eslint-disable @next/next/no-img-element */
'use client';

import Divider from '@mui/material/Divider';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';

export default function HomePage() {
  return (
    <Stack alignItems='center' spacing={2}>
      <Typography variant='h4'>Services</Typography>
      <img src='assets/services.png' alt='Our services' width='75%' height='auto' />
      <Typography variant='h4'>Areas of Service</Typography>
      <Stack direction='row' divider={<Divider orientation='vertical' flexItem />} spacing={2}>
        <Typography variant='h6'>Tulsa</Typography>
        <Typography variant='h6'>Broken Arrow</Typography>
        <Typography variant='h6'>Bixby</Typography>
        <Typography variant='h6'>Jenks</Typography>
      </Stack>
      <img src='assets/areas-of-service.png' alt='Areas of services' width='75%' height='auto' />
    </Stack>
  );
}
