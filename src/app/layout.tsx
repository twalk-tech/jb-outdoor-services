'use client';

import '../styles/globals.css';

import Box from '@mui/material/Box';
import Head from 'next/head';
import Typography from '@mui/material/Typography';
import logo from '../public/assets/logo-full.png';
import useMediaQuery from '../lib/hooks/media-query';

import { Navbar } from '@twalk-tech/react-lib/nextjs/components';
import { Route } from '@twalk-tech/react-lib/nextjs/utils';
import { ThemeProvider } from "@mui/material";
import { theme } from '../utils/theme';

export default function RootLayout({ children }) {
  const topMargin = useMediaQuery(600) ? -5 : 1.5;
  const title = 'JB Outdoor Services';

  const routes: Array<Route> = [
    {
      name: 'Home',
      paths: ['/home']
    },
    {
      name: 'About Us',
      paths: ['/about']
    },
    {
      name: 'Prices',
      paths: ['/prices']
    },
    {
      name: 'Gallery',
      paths: ['/gallery']
    },
    {
      name: 'Contact',
      paths: ['/contact']
    }
  ];

  return (
    <html>
      <head>
        <title>{title}</title>
      </head>
      <Head>
        <link rel='preload' href='assets/services.png' as='image' />
        <link rel='preload' href='assets/areas-of-service.png' as='image' />
        <link rel='preload' href='assets/testimony.png' as='image' />
        <link rel='preload' href='assets/prices.png' as='image' />
        <link rel='preload' href='assets/gallery.png' as='image' />
      </Head>
      <body>
        <ThemeProvider theme={theme}>
          <Navbar
            name={title}
            shortName={title}
            routes={routes}
            logo={{
              src: logo.src,
              height: 100,
              width: 151
            }}
            theme={theme}
          />
          <Box mt={topMargin} p={2} bgcolor='#5BA338' color='#000' sx={{ textAlign: 'center' }}>
            <Typography variant='h5' sx={{ fontWeight: 'bold' }}>JB Outdoor Services is your one stop solution for all your outdoor needs!</Typography>
          </Box>
          <Box m={2} p={2} sx={{ textAlign: 'center' }}>
            {children}
          </Box>
        </ThemeProvider>
      </body>
    </html>
  );
}
