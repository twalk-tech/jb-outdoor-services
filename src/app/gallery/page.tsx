/* eslint-disable @next/next/no-img-element */
export default function GalleryPage() {
  return (
    <img src='/assets/gallery.png' alt='Gallery' width='75%' height='auto' />
  );
}
