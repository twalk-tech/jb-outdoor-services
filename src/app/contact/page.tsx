/* eslint-disable @next/next/no-img-element */
'use client';

import { GenericForm } from '@twalk-tech/react-lib/nextjs/components';
import { GenericFormItem } from '@twalk-tech/react-lib/nextjs/utils';
import { GenericSnackbar } from '@twalk-tech/react-lib/nextjs/components';
import { MutableRefObject, useRef, useState } from 'react';

import Grid from '@mui/material/Grid';
import Link from 'next/link';
import SendIcon from '@mui/icons-material/Send';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';

import * as yup from 'yup';

type ContactFormProps = {
  setSnackbarMessage: (message: string) => void;
  snackbarRef: MutableRefObject<null>;
}

function SocialLinks() {
  return (
    <Stack direction='row' alignItems='center' spacing={2}>
      <Stack alignItems='center' spacing={2}>
        <Typography variant='h5'>Facebook</Typography>
        <img src='/assets/facebook.png' alt='Facebook' width={200} height={200} />
      </Stack>
      <Stack alignItems='center' spacing={2}>
        <Typography variant='h5'>Instagram</Typography>
        <img src='/assets/instagram.png' alt='Instagram' width={200} height={200} />
      </Stack>
    </Stack>
  );
}

function ContactInfo() {
  return (
    <>
      <Typography variant='h5'>Email</Typography>
      <Typography variant='h6'>
        <Link href='mailto:jboutdoorservicesok@gmail.com'>jboutdoorservicesok@gmail.com</Link>
      </Typography>
      <Typography variant='h5'>Phone</Typography>
      <Typography variant='h6'>(918) 344-2916</Typography>
    </>
  );
}

function ContactForm(props: ContactFormProps) {
  const phoneNumberRegex = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
  const fields: Array<GenericFormItem> = [
    { fcName: 'name', name: 'Name', type: 'text' },
    { fcName: 'phone_number', name: 'Phone Number', type: 'text' },
    { fcName: 'email_address', name: 'Email Address', type: 'text' },
    { fcName: 'message', name: 'Message', type: 'multiline' }
  ];

  const defaultValues = {
    name: '',
    phone_number: '',
    email_address: '',
    message: ''
  };

  const validationSchema = yup.object({
    name: yup.string().required('Name is required'),
    email_address: yup.string().email('Invalid email address'),
    phone_number: yup.string().matches(phoneNumberRegex, 'Must be in the format of (555) 555-5555'),
    message: yup.string().required('Message is required').max(750, 'Message must be less than 750 characters')
  });

  const sendEmail = async (values: any) => {
    if (values.email_address.length === 0 && values.phone_number.length === 0) {
      props.setSnackbarMessage('Must provide at least one contact method!');
      // @ts-ignore
      props.snackbarRef.current.open();
      return;
    }

    // Replace newlines with <br /> element so it renders in HTML email properly
    const message = (values.message as string).split('\n').join('<br />');
    const email = `<p><b>From:</b> ${values.name}</p>
<p><b>Phone Number:</b> ${values.phone_number}</p>
<p><b>Email Address:</b> ${values.email_address}</p>
<p>
  <b>Message:</b><br />
  <p style="margin-left: 30px">${message}</p>
</p>
    `

    const sent = await fetch('/api/send-email', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({'data': email})
    });

    const response = await sent.json();

    props.setSnackbarMessage(response['message']);
    // @ts-ignore
    props.snackbarRef.current.open();

    return;
  };

  return (
    <GenericForm
      title='Contact Us'
      fields={fields}
      defaultValues={defaultValues}
      validationSchema={validationSchema}
      onSubmit={sendEmail}
      submitButtonTitle={'Send Message'}
      submitButtonIcon={<SendIcon />}
      width='100%'
    />
  );
}

export default function ContactPage() {
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const snackbarRef = useRef(null);

  return (
    <>
      <Grid container spacing={4} columns={2} alignItems='top' justifyContent='center'>
        <Grid item sx={{ flexGrow: 1 }}>
          <Stack alignItems='center' spacing={2}>
            <SocialLinks />
            <ContactInfo />
          </Stack>
        </Grid>
        <Grid item sx={{ flexGrow: 1 }}>
          <ContactForm setSnackbarMessage={setSnackbarMessage} snackbarRef={snackbarRef} />
        </Grid>
      </Grid>
      <GenericSnackbar ref={snackbarRef} message={snackbarMessage} duration={5000} />
    </>
  );
}
