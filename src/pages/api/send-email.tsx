import { NextApiRequest, NextApiResponse } from 'next';

export default async function handle(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === 'POST') {
    await handlePOST(req, res);
  } else {
    res.status(405).end(`The HTTP ${req.method} method is not supported at this route.`);
  }
}

async function handlePOST(req: NextApiRequest, res: NextApiResponse) {
  const zohoWebhook = process.env.ZOHO_WEBHOOK;

  if (!zohoWebhook) {
    res.status(401).end('Not authenticated to send email!');
    return;
  }

  const sent = await fetch(zohoWebhook, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(req.body)
  });

  if (sent.status !== 200) {
    res.status(500).json({'message': 'Error sending email. Please try again.'});
  } else {
    res.status(sent.status).json({'message': 'Your email has been sent! Thank you for contacting JB Outdoor Services.'})
  }
}
