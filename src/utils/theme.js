import { createTheme } from '@mui/material/styles';

export const theme = createTheme({
  palette: {
    type: 'light',
    primary: {
      main: '#000'
    },
    secondary: {
      main: '#5BA338'
    }
  },
  typography: {
    fontFamily: 'Rockwell'
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: ({ theme }) => ({
          '&:hover': {
            backgroundColor: theme.palette.secondary.main + '!important'
          }
        })
      }
    }
  }
});
