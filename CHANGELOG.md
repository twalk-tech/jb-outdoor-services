# Changelog

## Version 1.1.2 - March 24, 2023
  - Protecting deployments

## Version 1.1.1 - March 24, 2023
  - Using server components to ensure images are preloaded
  - Using v4.1.3 of @twalk-tech/react-lib to fix navigation links on mobile

## Version 1.1.0 - March 21, 2023
  - Added contact form
  - Preloaded all images

## Version 1.0.0 - March 16, 2023
  - Combined Home and Services pages
  - Added email and phone number to contact page
  - Renamed testimony page to "About Us'

## Version 0.1.1 - March 15, 2023
  - Fixing job name

## Version 0.1.0 - March 15, 2023
  - Initial release
